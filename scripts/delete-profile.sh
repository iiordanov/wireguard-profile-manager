#!/bin/bash -e

function delete_profile() {
  local username=$1
  local profile_number=$2
  python3 ./src/wg-config.py profile delete --name "${username}" --id "${profile_number}"
}

username="$1"
profile_number="$2"

if [ -z "${username}" ] || [ -z "${profile_number}" ]
then
  echo "Usage $0 username profile_number"
  exit 1
fi

DIR="$(dirname $0)"
PRJ_DIR="$DIR/../"

pushd "${PRJ_DIR}" >&/dev/null

# Try to delete a profile matching the parameters
delete_profile "${username}" "${profile_number}"

