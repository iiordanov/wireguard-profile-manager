#!/bin/bash -e

umask 077

server_public_address="$1"
server_listen_port="$2"
mtu="$3"
allowed_ips="$4"

for utility in wg wg-quick jq
do
  if ! which "${utility}" >&/dev/null
  then
    echo "Please install wireguard, wireguard-tools, and jq"
    exit 1
  fi
done

if [ -z "${server_public_address}" ] || [ -z "${server_listen_port}" ] \
|| [ -z "${mtu}" ] || [ -z "${allowed_ips}" ]
then
  echo "Usage $0 server_public_address server_listen_port mtu allowed_ips"
  exit 2
fi

DIR="$(dirname $0)"
PRJ_DIR="$DIR/../"

jq '.server_public_address = '\"${server_public_address}\"\
'| .server_listen_port = '\"${server_listen_port}\"\
'| .mtu = '\"${mtu}\"\
'| .allowed_ips = '\""${allowed_ips}"\"\
 < "${PRJ_DIR}"/configs/server.json > /tmp/server_$$.json

mv /tmp/server_$$.json "${PRJ_DIR}"/configs/server.json

echo "Updated server config:"
echo
jq '.' < "${PRJ_DIR}"/configs/server.json

pushd "${PRJ_DIR}"
python3 ./src/wg-config.py server init_keys
python3 ./src/wg-config.py server create
popd

config="$(jq -r '.server_config_name' < configs/server.json)"
wg-quick down ./server/"${config}"
wg-quick up   ./server/"${config}"
