#!/bin/bash -e

function read_profile() {
  local username=$1
  local profile_number=$2
  python3 ./src/wg-config.py profile read --name "${username}" --id "${profile_number}"
}

username="$1"
profile_number="$2"

if [ -z "${username}" ] || [ -z "${profile_number}" ]
then
  echo "Usage $0 username profile_number"
  exit 1
fi

DIR="$(dirname $0)"
PRJ_DIR="$DIR/../"

pushd "${PRJ_DIR}" >&/dev/null

# Try to get a profile matching the parameters
PROFILE=$(read_profile "${username}" "${profile_number}")
if [ "${PROFILE}" == "PROFILE NOT FOUND" ]
then
  # If the profile is not found, create it and restart the server.
  python3 ./src/wg-config.py profile create --name "${username}" --id "${profile_number}" >& /dev/null

  python3 ./src/wg-config.py server create

  config="$(jq -r '.server_config_name' < configs/server.json)"
  wg-quick down ./server/"${config}" >& /dev/null || true
  wg-quick up   ./server/"${config}" >& /dev/null || true

  # Read the new profile
  PROFILE=$(read_profile "${username}" "${profile_number}")
fi

echo "${PROFILE}"

popd >&/dev/null
