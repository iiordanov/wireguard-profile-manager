import click
import json
import ipaddress
import string
import subprocess
import io
import os


SERVER_CONFIG="configs/server.json"
PEER_CONFIG="server/peers.json"

SERVER_TEMPLATE="templates/server.conf"
CLIENT_TEMPLATE="templates/client.conf"
SERVER_PEER_TEMPLATE="templates/server_peer.conf"


def read_server_config(file):
    server_config = json.loads(read_file_to_string(file))
    return server_config


def write_string_to_file(str, file):
    with open(file, "w") as f:
        f.write(str)


def get_wg_priv():
    proc = subprocess.Popen(["wg", "genkey"], stdout=subprocess.PIPE)
    for line in io.TextIOWrapper(proc.stdout, encoding="utf-8"):
        return line.strip()


def get_wg_psk():
    proc = subprocess.Popen(["wg", "genpsk"], stdout=subprocess.PIPE)
    for line in io.TextIOWrapper(proc.stdout, encoding="utf-8"):
        return line.strip()


def get_wg_pub(wg_priv):
    proc = subprocess.Popen(["wg", "pubkey"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    x = proc.communicate(bytes(wg_priv, "utf-8"))
    return x[0].decode("utf-8").strip()


def read_file_to_string(file):
    contents = None
    try:
        with open(file, "r") as f:
            contents = f.read()
    except FileNotFoundError:
        pass
    return contents

def get_profiles():
    profiles = {}
    try:
        with open(PEER_CONFIG, "r") as f:
            profiles = json.loads(f.read())
    except FileNotFoundError:
        pass
    return profiles


def write_profiles(profiles):
    with open(PEER_CONFIG, "w") as f:
        f.write(json.dumps(profiles, indent=4))


def get_nums(profiles):
    nums = []
    for user in profiles.keys():
        for id in profiles.get(user).keys():
            nums.append(profiles.get(user)[id]["num"])
    return nums


def detempletize(template, **kwargs):
    tmpl = read_file_to_string(template)
    return string.Template(tmpl).substitute(kwargs)


def get_num(existing):
    server_config = read_server_config(SERVER_CONFIG)
    prefixlen = int(server_config["server_subnet"])
    nums = 2**prefixlen

    for x in range(1, nums):
        if x not in sorted(existing):
            break
    return x


@click.group()
@click.pass_context
def cli(ctx, *args, **kwargs):
    ctx.ensure_object(dict)


@cli.group()
@click.pass_context
def profile(ctx, *args, **kwargs):
    ctx.ensure_object(dict)


@profile.command("create")
@click.option("--name", required=True)
@click.option("--id", required=True)
@click.option("--keep-alive", required=False, default=27)
@click.pass_context
def create(ctx, *args, **kwargs):
    click.echo(f"Creating profile {kwargs}")
    id = kwargs["id"]
    name = kwargs["name"]

    profiles = get_profiles()
    profiles_for_user = profiles.get(name, {})
    profile = profiles_for_user.get(id, {})
    num = profile.get("num", get_num(get_nums(profiles)))
    kwargs.update({"num": num})
    priv = get_wg_priv()
    kwargs.update({"priv": priv})
    kwargs.update({"pub": get_wg_pub(priv)})
    kwargs.update({"psk": get_wg_psk()})
    
    profiles_for_user[kwargs["id"]] = kwargs
    
    profiles.update({kwargs["name"]: profiles_for_user})

    write_profiles(profiles)
    

@profile.command("delete")
@click.option("--name", default=False)
@click.option("--id", required=True)
@click.pass_context
def delete(ctx, *args, **kwargs):
    click.echo(f"Deleting profile {kwargs}")
    id = kwargs["id"]
    name = kwargs["name"]

    profiles = get_profiles()
    if name not in profiles or id not in profiles[name]:
        print("PROFILE NOT FOUND")
        return

    profiles[name].pop(id)
    write_profiles(profiles)


def get_client_address(server_address, addr_num):
    addr = ipaddress.ip_address(server_address) + 1 + addr_num
    return addr


@profile.command("read")
@click.option("--name", default=False)
@click.option("--id", required=True)
@click.pass_context
def read(ctx, *args, **kwargs):
    profiles = get_profiles()
    profile = profiles.get(kwargs["name"], {}).get(kwargs["id"])
    server_config = read_server_config(SERVER_CONFIG)
    server_public_address = server_config["server_public_address"]
    server_port = server_config["server_listen_port"]
    server_address = server_config["server_address"]
    if not profile:
        print("PROFILE NOT FOUND")
        return

    addr_num = profile.get("num", None)
    addr = get_client_address(server_address, addr_num)
    profile.update({"address": addr})
    profile.update({"server_endpoint": f"{server_public_address}:{server_port}"})
    profile.update(server_config)
    print(detempletize(CLIENT_TEMPLATE, **profile))


@cli.group()
@click.pass_context
def server(*args, **kwargs):
    pass


def output_server_config(profiles):
    server_config = read_server_config(SERVER_CONFIG)
    server_address = server_config["server_address"]
    x = detempletize(SERVER_TEMPLATE, **server_config)
    for p in profiles:
        for i in profiles[p]:
            x += "\n"
            profile = profiles[p][i]
            addr_num = profile["num"]
            profile.update(server_config)
            addr = get_client_address(server_address, addr_num)
            profile.update({"addr": addr})
            x += detempletize(SERVER_PEER_TEMPLATE, **profile)
            x += "\n"

    cfg_path = os.path.join("server", server_config["server_config_name"])
    click.echo(f"Creating server config at {cfg_path}")
    write_string_to_file(x, cfg_path)


def update_server_config_values(**kwargs):
    server_config = read_server_config(SERVER_CONFIG)
    priv = get_wg_priv()
    kwargs.update({"server_private_key": priv})
    kwargs.update({"server_public_key": get_wg_pub(priv)})
    server_config.update(kwargs)
    write_string_to_file(json.dumps(server_config, indent=4), SERVER_CONFIG)


@server.command("init_keys")
@click.pass_context
def init_keys(ctx, *args, **kwargs):
    click.echo(f"Initializing server keys")
    update_server_config_values(**kwargs)


@server.command("create")
@click.pass_context
def create(ctx, *args, **kwargs):
    profiles = get_profiles()
    output_server_config(profiles)


if __name__ == '__main__':
    cli(obj={})
