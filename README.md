# wireguard-profile-manager

This utility creates, deletes and outputs wireguard profiles for a server to ease profile management
for a simple hub-spoke wireguard setup.

## Automatic Config

There are some helper scripts to get you started quickly.

### Set Server Up

```bash
./scripts/set-up-wireguard.sh 1.2.3.4 23435 1300 "10.0.0.0/24, 10.0.1.0/24, 10.0.2.0/24"
```

### Create Profiles

```bash
./scripts/get-profile.sh bob 0
wudo wg show
```

### Delete Profiles

```bash
./scripts/delete-profile.sh bob 0
sudo wg show
```

## Manual Config

Edit `server.json` and make sure to customize at least:

```json
{
    "server_public_address": "1.2.3.4",
    "server_private_key": "123ABC",
    "server_public_key": "234DEF",
    "server_listen_port": "23435",
    "mtu": "1300",
    "allowed_ips": "10.0.0.0/16, 10.10.0.0/16, 10.20.0.0/16, 10.30.0.0/16",
}
```

If running a local DNS server on your wireguard server and if changing `server_address`, also make sure to customize `server_dns` to match.


### Usage Help

To see all options, run:

```bash
python3 ./src/wg-config.py --help

```

### Init Server Config

Create server wireguard private and public keys.

```bash
python3 ./src/wg-config.py server init_keys

```

### Create User Profiles

Create at least one user profile.

```bash
python3 ./src/wg-config.py profile create --name "bob" --id "0"
python3 ./src/wg-config.py profile create --name "bob" --id "1"
python3 ./src/wg-config.py profile create --name "jane" --id "0"
python3 ./src/wg-config.py profile create --name "jane" --id "1"
python3 ./src/wg-config.py server create
```

### Delete User Profiles

You can delete a profile if you changed your mind.

```bash
python3 ./src/wg-config.py profile delete --name "bob" --id "1"
python3 ./src/wg-config.py profile delete --name "jane" --id "0"
python3 ./src/wg-config.py server create
```

### Create Server Config

Then, update server config.

```bash
python3 ./src/wg-config.py server create

```

### Start Server

```bash
sudo wg-quick up ./server/server_wg0.conf
sudo wg show server_wg0

```

### Stop Server

```bash
sudo wg-quick down ./server/server_wg0.conf

```

### Update Server Config

When a new profile is created, you'll need recreate config and to stop and start the server to load the new config, e.g.:

```bash
python3 ./src/wg-config.py profile create --name "bob" --id "0"

python3 ./src/wg-config.py server create

sudo wg-quick down ./server/server_wg0.conf
sudo wg-quick up ./server/server_wg0.conf
sudo wg show server_wg0

```

